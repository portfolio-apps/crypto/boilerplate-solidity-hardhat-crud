// import { expect } from "chai";
import { ethers } from "hardhat";

async function contractDeployAddEntries() {
  const Library = await ethers.getContractFactory("Library");
  const lib = await Library.deploy();
  await lib.deployed();
  let addBook;
  addBook = await lib.addBook("John Smith", "Learning Solidity");
  addBook = await lib.addBook("Jingle HeinmerSchmidt", "Learning Rust");
  addBook = await lib.addBook("Ryan Eggz", "Learning Ethereum");
  await addBook.wait();

  return lib;
}

describe("Library contract can...", function () {
  /** -----------------------------------------------------------
   * List of books also a method for filtering by a value
   * ------------------------------------------------------------
   */
  it("list books", async function () {
    const lib = await contractDeployAddEntries();

    const books = await lib.listBooks();
    console.log(books);
    // console.log(books.filter((book) => Number(book.id) === 2));
  });

  /** -----------------------------------------------------------
   * Fetch a book by its mapping id
   * ------------------------------------------------------------
   */
  it("fetch book by id", async function () {
    const lib = await contractDeployAddEntries();

    const book = await lib.findBookById(2);
    console.log(book);
  });

  /** -----------------------------------------------------------
   * Update book using its mapping id
   * ------------------------------------------------------------
   */
  it("update book by id", async function () {
    const lib = await contractDeployAddEntries();

    const bookIndex = 1;

    // Update book at id
    await lib.updateBook(bookIndex, "John Jacob", "It's late af");
    // Find the updated book
    const book = await lib.findBookById(bookIndex);

    console.log(book);
  });

  /** -----------------------------------------------------------
   * Delete book using its mapping id
   * ------------------------------------------------------------
   */
  it("delete book by index", async function () {
    const lib = await contractDeployAddEntries();

    const bookIndex = 1;

    // Delete book at id
    await lib.deleteBookById(bookIndex);
    // Check mapping thats deleted
    const book = await lib.findBookById(bookIndex);
    console.log(book);

    // Delete from list
    await lib.deleteBookFromMappedByIndex(bookIndex - 1);
    const books = await lib.listBooks();
    console.log(books);
  });
});
