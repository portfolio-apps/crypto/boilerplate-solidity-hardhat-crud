//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract Library {

    uint public index = 0;

    struct Book { 
        string title;
        string author;
    }
    mapping (uint => Book) public books;

    struct MappedBook {
        uint id; 
        string title;
        string author;
    }
    MappedBook[] public lib;

    // Adds book to mapping
    function addBook(
        string memory _author,
        string memory _title
    ) public {
        // Incrementing Key
        index += 1;
        // Holds the list of books used for querying
        MappedBook memory mappedBook = MappedBook(index, _author, _title);
        lib.push(mappedBook);
        // Creates a mapping for fetching single book with an id
        Book memory book = Book(_author, _title);
        books[index] = book;
    }

     // Adds book to mapping
    function updateBook(
        uint _id,
        string memory _author,
        string memory _title
    ) public {
        if (bytes(_author).length > 0) {
            books[_id].author = _author;
        }
        if (bytes(_title).length > 0) {
            books[_id].title = _title;
        }
    }

    // Get book count
    function countBooks() public view returns (uint) {
        return index;
    }
    // List of books
    function listBooks() public view returns (MappedBook[] memory) {
        return lib;
    }
    // List of books
    function findBookById(uint _id) public view returns (Book memory) {
        return books[_id];
    }
    // Delete book
    function deleteBookById(uint _id) public {
        delete books[_id];
    }
    // Deletes book from mapped array
    function deleteBookFromMappedByIndex(uint _index) public {
        delete lib[_index];
    }

    // function measureBytesOfSTitleAtIndex(uint _index) public view returns (uint256) {
    //     Book memory foundBook = findBookByIndex(_index);
    //     return bytes(foundBook.title).length;
    // }

    // function measureBytesOfSAuthorAtIndex(uint _index) public view returns (uint256) {
    //     Book memory foundBook = findBookByIndex(_index);
    //     return bytes(foundBook.author).length;
    // }
}
